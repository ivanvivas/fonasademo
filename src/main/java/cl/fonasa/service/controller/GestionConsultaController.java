package cl.fonasa.service.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.fonasa.service.dto.ConsultaDto;
import cl.fonasa.service.dto.PacienteDto;
import cl.fonasa.service.service.GestionConsultasService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("GestionConsultas/")
@CrossOrigin(origins = "*")
public class GestionConsultaController {
	
	@Autowired
    private GestionConsultasService service;
	
	/**
    * Listar Pacientes Fumadores Urgentes
    * 
    * @return List<PacienteDto>
    */
	@GetMapping(
		value = "/listarPacientesFumadoresUrgentes", 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public List<PacienteDto> listarPacientesFumadoresUrgentes(){
		log.info("GestionConsulta.listarPacientesFumadoresUrgentes");
		
		return service.listarPacientesFumadoresUrgentes();
	}
	
	/**
    * Obtener paciente mas anciano en espera
    *  
    * @return List<PacienteDto>
    */
	@GetMapping(
		value = "/pacienteMasAncianoEnEspera", 
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public List<PacienteDto> pacienteMasAncianoEnEspera(){
		log.info("GestionConsulta.pacienteMasAncianoEnEspera");
		
		return service.pacienteMasAncianoEnEspera();
	}
	
	/**
	 * Ingresar paciente
	 * 
	 * @param PacienteDto
	 * @return PacienteDto
	 */
    @PostMapping(
		value = "/ingresarPaciente",
		produces = MediaType.APPLICATION_JSON_VALUE,
		consumes = MediaType.APPLICATION_JSON_VALUE
	)
    public PacienteDto ingresarPaciente(@RequestBody @Valid PacienteDto requestDto){
    	log.info("GestionConsulta.ingresarPaciente");
    	
    	return service.ingresarPaciente(requestDto);
    }
    
    /**
	 * Listar Pacientes Mayor Riego
	 * 
	 * @param PacienteDto
	 * @return PacienteDto
	 */
    @PostMapping(
		value = "/listarPacientesMayorRiesgo",
		produces = MediaType.APPLICATION_JSON_VALUE,
		consumes = MediaType.APPLICATION_JSON_VALUE
	)
    public List<PacienteDto> listarPacientesMayorRiesgo(@RequestBody @Valid PacienteDto requestDto){
    	log.info("GestionConsulta.listarPacientesMayorRiesgo");
    	
    	return service.listarPacientesMayorRiesgo(requestDto.getHistoriaClinica());
    }
    
    /**
     * Obtener consulta con mas pacientes atendidos
     *  
     * @return List<ConsultaDto>
     */
 	@GetMapping(
 		value = "/consultaMasPacientesAtendidos", 
 		produces = MediaType.APPLICATION_JSON_VALUE
 	)
 	public List<ConsultaDto> consultaMasPacientesAtendidos(){
 		log.info("GestionConsulta.consultaMasPacientesAtendidos");
 		
 		return service.consultaMasPacientesAtendidos();
 	}
 	
 	/**
	 * Atender pacientes que estan en espera
	 * 
	 * @return void
	 */
    @PostMapping(
		value = "/atenderPacientes"
	)
    public void atenderPacientes(){
    	log.info("GestionConsulta.atenderPacientes");
    	
    	service.atenderPacientes();
    }
    
    /**
	 * Liberar Consultas
	 * 
	 * @return void
	 */
    @PostMapping(
		value = "/liberarConsultas"
	)
    public void liberarConsultas(){
    	log.info("GestionConsulta.liberarConsultas");
    	
    	service.liberarConsultas();
    }
    
    /**
     * Obtener pacientes en espera por orden de llegada
     *  
     * @return List<PacienteDto>
     */
 	@GetMapping(
 		value = "/listarPacientesEnEspera", 
 		produces = MediaType.APPLICATION_JSON_VALUE
 	)
 	public List<PacienteDto> pacientesEnEspera(){
 		log.info("GestionConsulta.pacientesEnEspera");
 		
 		return service.pacientesEnEspera();
 	}
}
