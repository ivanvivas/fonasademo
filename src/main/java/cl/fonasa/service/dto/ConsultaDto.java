package cl.fonasa.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConsultaDto {
	private Integer idConsulta;
	private Integer cantPacientes;
	private String nombreEspecialista;
	private String tipoConsulta;
	private String estado;
	private Integer idHospital;
}
