package cl.fonasa.service.dto;

import java.util.Calendar;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PacienteDto {
	private Integer idPaciente;
	private String nombre;
	private Integer edad;
	private Integer historiaClinica;
	private Integer idHospital;
	private Boolean tieneDieta;
	private Integer relacionPesEst;
	private Boolean fumador;
	private Integer aniosFumador;
	private Double prioridad;
	private Double riesgo;
	private Calendar fechaLlegada;
	private Boolean fueAtendido;
	private Integer idConsulta;
}
