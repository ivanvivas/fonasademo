package cl.fonasa.service.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Configuracion de JPA
 */
@Configuration
@EnableJpaRepositories(basePackages = { "cl.fonasa.service.repository" },
repositoryImplementationPostfix = "ExtensionImpl"
)
public class JpaDbConfiguration {

}
