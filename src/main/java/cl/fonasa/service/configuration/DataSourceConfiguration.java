package cl.fonasa.service.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.core.env.Environment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class DataSourceConfiguration {
	@Value("${dataSource.driverClassName}")
	private String postgresqlDriver;
	
	@Value("${dataSource.url}")
	private String dsUrl;
	
	@Value("${dataSource.user}")
	private String dsUser;
	
	@Value("${dataSource.password}")
	private String dsPass;
	
	@Value("${dataSource.validationQuery}")
	private String dsValidationQuery;
	
	@Value("${dataSource.minIdle}")
	private Integer dsMinIdle;
	
	@Value("${dataSource.maxActive}")
	private Integer dsMaxActive;
	/**
	 * Crea una instancia del data source con scope prototype (no singleton)
	 * @return Instancia del data source
	 */
	@Bean
	@Scope("prototype")
	public DataSource dataSource() {
		DataSource ds = DataSourceBuilder.create()
	    		.driverClassName(postgresqlDriver)
	    		.url(dsUrl)
	    		.username(dsUser).password(dsPass)
	    		.build();
	    
		//Configura que tenga capacidad de reconexión
	    if (ds instanceof org.apache.tomcat.jdbc.pool.DataSource) {
	    	org.apache.tomcat.jdbc.pool.DataSource tomcatDataSource = (org.apache.tomcat.jdbc.pool.DataSource) ds;
	    	tomcatDataSource.setTestOnBorrow(true);
	    	tomcatDataSource.setValidationQuery(dsValidationQuery);
	    	tomcatDataSource.setMinIdle(dsMinIdle);
	    	tomcatDataSource.setMaxActive(dsMaxActive);
	    }
	    return ds;
	}
}
