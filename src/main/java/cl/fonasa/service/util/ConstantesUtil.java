package cl.fonasa.service.util;

public class ConstantesUtil {
	public static final String ESTADO_CONSULTA_LIBRE = "libre";
	public static final String ESTADO_CONSULTA_OCUPADA = "ocupada";
	public static final String TIPO_CONSULTA_PEDIATRIA = "pediatria";
	public static final String TIPO_CONSULTA_URGENCIA = "urgencia";
	public static final String TIPO_CONSULTA_CGI = "cgi";
}
