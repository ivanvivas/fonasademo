package cl.fonasa.service.util;

import java.util.Calendar;

import lombok.NoArgsConstructor;

/**
 * Funciones utilitarias generales
 * 
 * @author exivima
 *
 */
@NoArgsConstructor
public class HelperUtil {
	
	/**
	 * Obtener fecha de hoy tipo Calendar
	 * 
	 * @return fechaDeHoy
	 */
	public static Calendar fechaActual() {
		Calendar fechaActual = Calendar.getInstance();
		return fechaActual;
	}
}
