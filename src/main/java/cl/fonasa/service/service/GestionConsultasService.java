package cl.fonasa.service.service;

import java.util.List;

import cl.fonasa.service.dto.ConsultaDto;
import cl.fonasa.service.dto.PacienteDto;

public interface GestionConsultasService {
	/**
	 * Interface de servicio de listar paciente de mayor riesgo
	 * 
	 * @return List<ConsultaModel>
	 */
	public List<PacienteDto> listarPacientesFumadoresUrgentes();

	/**
	 * Interface de servicio de ingresar paciente
	 * 
	 * @param requestDto
	 * @return PacienteDto
	 */
	public PacienteDto ingresarPaciente(PacienteDto requestDto);

	/**
	 * Interface de servicio de obtener paciente mas anciano en espera
	 * 
	 * @return PacienteDto
	 */
	public List<PacienteDto> pacienteMasAncianoEnEspera();

	/**
	 * Interface de servicio para listar Pacientes de Mayor Risego
	 * 
	 * @param historiaClinica
	 * @return List<PacienteDto>
	 */
	public List<PacienteDto> listarPacientesMayorRiesgo(Integer historiaClinica);
	
	/**
	 * Interface de servicio para consulta de Mas Pacientes Atendidos
	 * 
	 * @return ConsultaDto
	 */
	public List<ConsultaDto> consultaMasPacientesAtendidos();

	/**
	 * Interface de servicio para atender pacientes
	 * 
	 * @return void
	 */
	public void atenderPacientes();

	/**
	 * Interface de servicio para liberar consultas
	 * 
	 * @return void
	 */
	public void liberarConsultas();

	/**
	 * Interface de servicio para listar pacientes en espera
	 * 
	 * @return List<PacienteDto>
	 */
	public List<PacienteDto> pacientesEnEspera();
}
