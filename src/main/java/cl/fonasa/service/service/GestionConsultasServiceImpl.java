package cl.fonasa.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import cl.fonasa.service.dto.ConsultaDto;
import cl.fonasa.service.dto.PacienteDto;
import cl.fonasa.service.model.ConsultaModel;
import cl.fonasa.service.model.PacienteModel;
import cl.fonasa.service.repository.ConsultaRepository;
import cl.fonasa.service.repository.PacienteRepository;
import cl.fonasa.service.util.ConstantesUtil;
import cl.fonasa.service.util.HelperUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * Clase para implementaciones de servicio
 * 
 * @author exivima
 *
 */
@Slf4j
@Service
public class GestionConsultasServiceImpl implements GestionConsultasService {
	@Autowired
	private ConsultaRepository consultaRepository;
	
	@Autowired
	private PacienteRepository pacienteRepository;
	
	@Override
	public List<PacienteDto> listarPacientesFumadoresUrgentes() {
		
		List<PacienteDto> pacientesList = pacienteRepository.listarPacientesFumadoresUrgentes();
		
		return pacientesList;
	}

	/**
	 * Implemenacion de servicio de ingresar paciente
	 */
	@Override
	public PacienteDto ingresarPaciente(PacienteDto pacienteRequest) throws HttpServerErrorException {
		// Crear instancia de modelo de paciente a partir del request
		PacienteModel paciente = PacienteModel.builder()
				.aniosFumador(pacienteRequest.getAniosFumador())
				.edad(pacienteRequest.getEdad())
				.fechaLlegada(pacienteRequest.getFechaLlegada())
				.fumador(pacienteRequest.getFumador())
				.idHospital(pacienteRequest.getIdHospital())
				.nHistoriaClinica(pacienteRequest.getHistoriaClinica())
				.nombre(pacienteRequest.getNombre())
				.tieneDieta(pacienteRequest.getTieneDieta())
				.relacionPesEst(pacienteRequest.getRelacionPesEst())
				.prioridad(calcularPrioridad(pacienteRequest))
				.fechaLlegada(HelperUtil.fechaActual())
				.fueAtendido(Boolean.FALSE)
				.build();
		
		// Calcular y asignar riesgo del paciente
		paciente.setRiesgo(calcularRiesgo(pacienteRequest, paciente.getPrioridad()));
		
		// Guardar regitro en la BD
		PacienteModel pacienteSaved;
		try {
			pacienteSaved = pacienteRepository.save(paciente);
		}
		catch (Exception ex) {
			log.info("Error en persistencia: " + ex.getMessage());
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Error en persistencia");
		}
		
		// Retornar datos del paciente ingresado
		PacienteDto pacienteDto = PacienteDto.builder()
				.nombre(pacienteSaved.getNombre())
				.idPaciente(pacienteSaved.getIdPaciente())
				.build();
		
		return pacienteDto;
	}

	private Double calcularPrioridad(PacienteDto paciente) {
		Double prioridad = 0.0;
		
		// Paciente niño
		if (paciente.getEdad() >= 1 && paciente.getEdad() <= 15) {
			if (paciente.getEdad() >= 1 && paciente.getEdad() <= 5) {
				prioridad = paciente.getRelacionPesEst() + 3.0;
			}
			else if (paciente.getEdad() >= 6 && paciente.getEdad() <= 12) {
				prioridad = paciente.getRelacionPesEst() + 2.0;
			}
			else if (paciente.getEdad() >= 13 && paciente.getEdad() <= 15) {
				prioridad = paciente.getRelacionPesEst() + 1.0;
			}
		}
		// Paciente joven
		else if (paciente.getEdad() >= 16 && paciente.getEdad() <= 40) {
			if (paciente.getFumador()) {
				prioridad = (paciente.getAniosFumador() / 4.0) + 2.0;
			}
			else {
				prioridad = 2.0;
			}	
		}
		// Paciente anciano
		else if (paciente.getEdad() >= 41) {
			if (paciente.getTieneDieta()) {
				prioridad = (paciente.getEdad() / 20.0) + 4.0;
			}
			else {
				prioridad = (paciente.getEdad() / 30.0) + 3.0;
			}
		}
		
		return prioridad;
	}
	
	private Double calcularRiesgo(PacienteDto paciente, Double prioridad) {
		Double riesgo = 0.0;
				
		if (paciente.getEdad() >= 41 ) {
			riesgo = ((paciente.getEdad() * prioridad) / 100.0) + 5.3;
		}
		else {
			riesgo = (paciente.getEdad() * prioridad) / 100.0;
		}
		
		return riesgo;
	}

	/**
	 * Implemenacion de servicio de obtener paciente mas anciano en espera
	 */
	@Override
	public List<PacienteDto> pacienteMasAncianoEnEspera() {
		return pacienteRepository.pacienteMasAnciano();
	}

	/**
	 * Implemenacion de servicio para listar Pacientes de Mayor Riesgo
	 */
	@Override
	public List<PacienteDto> listarPacientesMayorRiesgo(Integer historiaClinica) {
		return pacienteRepository.listarPacientesMayorRiego(historiaClinica);
	}

	/**
	 * Implemenacion de servicio para obtener consulta de Mas Pacientes Atendidos
	 */
	@Override
	public List<ConsultaDto> consultaMasPacientesAtendidos() {
		return consultaRepository.consultaMasPacientesAtendidos();
	}

	/**
	 * Implemenacion de servicio para atender Pacientes
	 */
	@Override
	public void atenderPacientes() {
		List<PacienteModel> pacientesList = pacienteRepository.listarPacientesPorPrioridad();
		
		// Recorrer lista para asignar pacientes a consultas segun logica de atencion
		for (PacienteModel paciente : pacientesList) {
			// Verificar la disponibilidad de consultas
			ConsultaModel consulta = consultaRepository.obtenerConsultaDisponible(
					paciente.getIdHospital(), 
					obtenerTipoConsulta(paciente));
			/* 
			 * Si hay consulta disponible, entonces asignar paciente a consulta correspondiente
			 * y actualizar estados
			 */
			if (consulta != null) {
				// Actualizar consulta
				consulta.setEstado(ConstantesUtil.ESTADO_CONSULTA_OCUPADA);
				consulta.setCantPacientes(consulta.getCantPacientes() + 1);
				consultaRepository.save(consulta);
				
				// Actualizar paciente
				paciente.setFueAtendido(Boolean.TRUE);
				paciente.setIdConsulta(consulta.getIdConsulta());
				pacienteRepository.save(paciente);
			}
		}
	}
	
	private String obtenerTipoConsulta(PacienteModel paciente) {
		String tipoConsulta = "";
		// Paciente niño
		if (paciente.getEdad() >= 1 && paciente.getEdad() <= 15) {
			if (paciente.getPrioridad() <= 4.0) {
				tipoConsulta = ConstantesUtil.TIPO_CONSULTA_PEDIATRIA;
			}
			else {
				tipoConsulta = ConstantesUtil.TIPO_CONSULTA_URGENCIA;
			}
		// Si es joven o anciano
		} else if (paciente.getEdad() >= 16) {
			if (paciente.getPrioridad() <= 4.0) {
				tipoConsulta = ConstantesUtil.TIPO_CONSULTA_CGI;
			}
			else {
				tipoConsulta = ConstantesUtil.TIPO_CONSULTA_URGENCIA;
			}
		}
		
		return tipoConsulta;
		
	}

	/**
	 * Implemenacion de servicio para liberar consultas
	 */
	@Override
	public void liberarConsultas() {
		// Listar consultas ocupadas
		List<ConsultaModel> consultasList = consultaRepository.listarConsultasOcupadas();
		
		// Recorrer lista y liberar cada consulta
		for (ConsultaModel consulta : consultasList) {
			consulta.setEstado(ConstantesUtil.ESTADO_CONSULTA_LIBRE);
			consultaRepository.save(consulta);
		}
		
		// Atender a nuevos pacientes
		atenderPacientes();
	}

	@Override
	public List<PacienteDto> pacientesEnEspera() {
		return pacienteRepository.listarPacientesEnEspera();
	}
}
