package cl.fonasa.service.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import cl.fonasa.service.dto.PacienteDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = "listarPacientesFumadoresUrgentes",
		classes = @ConstructorResult(
			targetClass = PacienteDto.class,
			columns = {
				@ColumnResult(name = "id_paciente", type = Integer.class),
				@ColumnResult(name = "nombre", type = String.class),
				@ColumnResult(name = "edad", type = Integer.class),
				@ColumnResult(name = "no_historia_clinica", type = Integer.class),
				@ColumnResult(name = "id_hospital", type = Integer.class),
				@ColumnResult(name = "tiene_dieta", type = Boolean.class),
				@ColumnResult(name = "relacion_pes_est", type = Integer.class),
				@ColumnResult(name = "fumador", type = Boolean.class),
				@ColumnResult(name = "anios_fumador", type = Integer.class),
				@ColumnResult(name = "prioridad", type = Double.class),
				@ColumnResult(name = "riesgo", type = Double.class),
				@ColumnResult(name = "fecha_llegada", type = Calendar.class),
				@ColumnResult(name = "fue_atendido", type = Boolean.class),
				@ColumnResult(name = "id_consulta", type = Integer.class)
			}
		)
	),
	@SqlResultSetMapping(
		name = "pacienteMasAnciano",
		classes = @ConstructorResult(
			targetClass = PacienteDto.class,
			columns = {
				@ColumnResult(name = "id_paciente", type = Integer.class),
				@ColumnResult(name = "nombre", type = String.class),
				@ColumnResult(name = "edad", type = Integer.class),
				@ColumnResult(name = "no_historia_clinica", type = Integer.class),
				@ColumnResult(name = "id_hospital", type = Integer.class),
				@ColumnResult(name = "tiene_dieta", type = Boolean.class),
				@ColumnResult(name = "relacion_pes_est", type = Integer.class),
				@ColumnResult(name = "fumador", type = Boolean.class),
				@ColumnResult(name = "anios_fumador", type = Integer.class),
				@ColumnResult(name = "prioridad", type = Double.class),
				@ColumnResult(name = "riesgo", type = Double.class),
				@ColumnResult(name = "fecha_llegada", type = Calendar.class),
				@ColumnResult(name = "fue_atendido", type = Boolean.class),
				@ColumnResult(name = "id_consulta", type = Integer.class)
			}
		)
	),
	@SqlResultSetMapping(
		name = "listarPacientesMayorRiego",
		classes = @ConstructorResult(
			targetClass = PacienteDto.class,
			columns = {
				@ColumnResult(name = "id_paciente", type = Integer.class),
				@ColumnResult(name = "nombre", type = String.class),
				@ColumnResult(name = "edad", type = Integer.class),
				@ColumnResult(name = "no_historia_clinica", type = Integer.class),
				@ColumnResult(name = "id_hospital", type = Integer.class),
				@ColumnResult(name = "tiene_dieta", type = Boolean.class),
				@ColumnResult(name = "relacion_pes_est", type = Integer.class),
				@ColumnResult(name = "fumador", type = Boolean.class),
				@ColumnResult(name = "anios_fumador", type = Integer.class),
				@ColumnResult(name = "prioridad", type = Double.class),
				@ColumnResult(name = "riesgo", type = Double.class),
				@ColumnResult(name = "fecha_llegada", type = Calendar.class),
				@ColumnResult(name = "fue_atendido", type = Boolean.class),
				@ColumnResult(name = "id_consulta", type = Integer.class)
			}
		)
	),
	@SqlResultSetMapping(
		name = "listarPacientesPorPrioridad",
		classes = @ConstructorResult(
			targetClass = PacienteModel.class,
			columns = {
				@ColumnResult(name = "id_paciente", type = Integer.class),
				@ColumnResult(name = "nombre", type = String.class),
				@ColumnResult(name = "edad", type = Integer.class),
				@ColumnResult(name = "no_historia_clinica", type = Integer.class),
				@ColumnResult(name = "id_hospital", type = Integer.class),
				@ColumnResult(name = "tiene_dieta", type = Boolean.class),
				@ColumnResult(name = "relacion_pes_est", type = Integer.class),
				@ColumnResult(name = "fumador", type = Boolean.class),
				@ColumnResult(name = "anios_fumador", type = Integer.class),
				@ColumnResult(name = "prioridad", type = Double.class),
				@ColumnResult(name = "riesgo", type = Double.class),
				@ColumnResult(name = "fecha_llegada", type = Calendar.class),
				@ColumnResult(name = "fue_atendido", type = Boolean.class),
				@ColumnResult(name = "id_consulta", type = Integer.class)
			}
		)
	),
	@SqlResultSetMapping(
		name = "listarPacientesPorLlegada",
		classes = @ConstructorResult(
			targetClass = PacienteModel.class,
			columns = {
				@ColumnResult(name = "id_paciente", type = Integer.class),
				@ColumnResult(name = "nombre", type = String.class),
				@ColumnResult(name = "edad", type = Integer.class),
				@ColumnResult(name = "no_historia_clinica", type = Integer.class),
				@ColumnResult(name = "id_hospital", type = Integer.class),
				@ColumnResult(name = "tiene_dieta", type = Boolean.class),
				@ColumnResult(name = "relacion_pes_est", type = Integer.class),
				@ColumnResult(name = "fumador", type = Boolean.class),
				@ColumnResult(name = "anios_fumador", type = Integer.class),
				@ColumnResult(name = "prioridad", type = Double.class),
				@ColumnResult(name = "riesgo", type = Double.class),
				@ColumnResult(name = "fecha_llegada", type = Calendar.class),
				@ColumnResult(name = "fue_atendido", type = Boolean.class),
				@ColumnResult(name = "id_consulta", type = Integer.class)
			}
		)
	)
})
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "paciente")
public class PacienteModel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_paciente", unique=true, updatable = false, nullable = false)
	private Integer idPaciente;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="edad")
	private Integer edad;
	
	@Column(name="no_historia_clinica")
	private Integer nHistoriaClinica;
	
	@Column(name="id_hospital")
	private Integer idHospital;
	
	@Column(name="tiene_dieta")
	private Boolean tieneDieta;
	
	@Column(name="relacion_pes_est")
	private Integer relacionPesEst;
	
	@Column(name="fumador")
	private Boolean fumador;
	
	@Column(name="anios_fumador")
	private Integer aniosFumador;
	
	@Column(name="prioridad")
	private Double prioridad;
	
	@Column(name="riesgo")
	private Double riesgo;
	
	@Column(name="fecha_llegada")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar fechaLlegada;
	
	@Column(name="fue_atendido")
	private Boolean fueAtendido;
	
	@Column(name="id_consulta")
	private Integer idConsulta;
}
