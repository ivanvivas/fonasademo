package cl.fonasa.service.model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import cl.fonasa.service.dto.ConsultaDto;
import cl.fonasa.service.dto.DisponiblesDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SqlResultSetMappings({
	@SqlResultSetMapping(
		name = "consultaMasPacientesAtendidos",
		classes = @ConstructorResult(
			targetClass = ConsultaDto.class,
			columns = {
				@ColumnResult(name = "id_consulta", type = Integer.class),
				@ColumnResult(name = "cant_pacientes", type = Integer.class),
				@ColumnResult(name = "nombre_especialista", type = String.class),
				@ColumnResult(name = "tipo_consulta", type = String.class),
				@ColumnResult(name = "estado", type = String.class),
				@ColumnResult(name = "id_hospital", type = Integer.class)
			}
		)
	),
	@SqlResultSetMapping(
		name = "consultasDisponibles",
		classes = @ConstructorResult(
			targetClass = DisponiblesDto.class,
			columns = {
				@ColumnResult(name = "disponibles", type = Integer.class)
			}
		)
	),
	@SqlResultSetMapping(
		name = "consultaDisponible",
		classes = @ConstructorResult(
			targetClass = ConsultaModel.class,
			columns = {
				@ColumnResult(name = "id_consulta", type = Integer.class),
				@ColumnResult(name = "cant_pacientes", type = Integer.class),
				@ColumnResult(name = "nombre_especialista", type = String.class),
				@ColumnResult(name = "tipo_consulta", type = String.class),
				@ColumnResult(name = "estado", type = String.class),
				@ColumnResult(name = "id_hospital", type = Integer.class)
			}
		)
	),
	@SqlResultSetMapping(
		name = "consultasOcupadas",
		classes = @ConstructorResult(
			targetClass = ConsultaModel.class,
			columns = {
				@ColumnResult(name = "id_consulta", type = Integer.class),
				@ColumnResult(name = "cant_pacientes", type = Integer.class),
				@ColumnResult(name = "nombre_especialista", type = String.class),
				@ColumnResult(name = "tipo_consulta", type = String.class),
				@ColumnResult(name = "estado", type = String.class),
				@ColumnResult(name = "id_hospital", type = Integer.class)
			}
		)
	)
})
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "consulta")
public class ConsultaModel {
	@Id
	@Column(name="id_consulta")
	private Integer idConsulta;
	
	@Column(name="cant_pacientes")
	private Integer cantPacientes;
	
	@Column(name="nombre_especialista")
	private String nombreEspecialista;
	
	@Column(name="tipo_consulta")
	private String tipoConsulta;
	
	@Column(name="estado")
	private String estado;
	
	@Column(name="id_hospital")
	private Integer idHospital;
}
