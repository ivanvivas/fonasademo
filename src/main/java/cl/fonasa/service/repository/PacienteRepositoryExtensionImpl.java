package cl.fonasa.service.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

import cl.fonasa.service.dto.PacienteDto;
import cl.fonasa.service.model.PacienteModel;
import lombok.extern.slf4j.Slf4j;

/**
 * Clase para implementaciones de interfaces de repository
 * 
 * @author exivima
 *
 */
@Slf4j
public class PacienteRepositoryExtensionImpl implements PacienteRepositoryExtension {
	@Autowired
	private EntityManager em;

	/**
	 * Implementacion de interface de repository de listarPacientesFumadoresUrgentes
	 */
	@Override
	public List<PacienteDto> listarPacientesFumadoresUrgentes() {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("select id_paciente, nombre, edad, no_historia_clinica, id_hospital, ");
		queryStr.append("tiene_dieta, relacion_pes_est, fumador, anios_fumador, prioridad, ");
		queryStr.append("riesgo, fecha_llegada, fue_atendido, id_consulta ");
		queryStr.append("from paciente where prioridad >= 4.0 and fue_atendido = false ");
		queryStr.append("order by prioridad desc");
		
		Query query = em.createNativeQuery(queryStr.toString(), "listarPacientesFumadoresUrgentes");
		
		List<PacienteDto> pacientesList = null;
		try {
			pacientesList = query.getResultList();
		}
		catch(Exception ex) {
			log.info("Error en persistencia: " + ex.getMessage());
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Error en persistencia");
		}
		
		return pacientesList;
	}

	/**
	 * Implementacion de interface de repository de pacienteMasAnciano
	 */
	@Override
	public List<PacienteDto> pacienteMasAnciano() {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("select id_paciente, nombre, edad, no_historia_clinica, id_hospital, ");
		queryStr.append("tiene_dieta, relacion_pes_est, fumador, anios_fumador, prioridad, ");
		queryStr.append("riesgo, fecha_llegada, fue_atendido, id_consulta ");
		queryStr.append("from paciente where fue_atendido = false ");
		queryStr.append("order by edad desc limit 1");
		
		Query query = em.createNativeQuery(queryStr.toString(), "pacienteMasAnciano");
		
		List<PacienteDto> pacientesList = null;
		try {
			pacientesList = query.getResultList();
		}
		catch(Exception ex) {
			log.info("Error en persistencia: " + ex.getMessage());
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Error en persistencia");
		}
		
		return pacientesList;
	}

	/**
	 * Implementacion de interface de repository de listarPacientesMayorRiego
	 */
	@Override
	public List<PacienteDto> listarPacientesMayorRiego(Integer historiaClinica) {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("select id_paciente, nombre, edad, no_historia_clinica, id_hospital, ");
		queryStr.append("tiene_dieta, relacion_pes_est, fumador, anios_fumador, prioridad, ");
		queryStr.append("riesgo, fecha_llegada, fue_atendido, id_consulta ");
		queryStr.append("from paciente where riesgo > (select riesgo from paciente where no_historia_clinica = ").append(historiaClinica).append(") ");
		queryStr.append("order by riesgo desc");
		
		Query query = em.createNativeQuery(queryStr.toString(), "listarPacientesMayorRiego");
		List<PacienteDto> pacientesList = null;
		try {
			pacientesList = query.getResultList();
		}
		catch(Exception ex) {
			log.info("Error en persistencia: " + ex.getMessage());
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Error en persistencia");
		}
		
		return pacientesList;
	}
	
	/**
	 * Implementacion de interface de repository de listarPacientesPorPrioridad no atendidos
	 */
	@Override
	public List<PacienteModel> listarPacientesPorPrioridad() {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("select id_paciente, nombre, edad, no_historia_clinica, id_hospital, ");
		queryStr.append("tiene_dieta, relacion_pes_est, fumador, anios_fumador, prioridad, ");
		queryStr.append("riesgo, fecha_llegada, fue_atendido, id_consulta ");
		queryStr.append("from paciente where fue_atendido = false ");
		queryStr.append("order by prioridad desc");
		
		Query query = em.createNativeQuery(queryStr.toString(), "listarPacientesPorPrioridad");
		List<PacienteModel> pacientesList = null;
		try {
			pacientesList = query.getResultList();
		}
		catch(Exception ex) {
			log.info("Error en persistencia: " + ex.getMessage());
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Error en persistencia");
		}
		
		return pacientesList;
	}

	/**
	 * Implementacion de interface de repository de pacientesEnEspera
	 */
	@Override
	public List<PacienteDto> listarPacientesEnEspera() {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("select id_paciente, nombre, edad, no_historia_clinica, id_hospital, ");
		queryStr.append("tiene_dieta, relacion_pes_est, fumador, anios_fumador, prioridad, ");
		queryStr.append("riesgo, fecha_llegada, fue_atendido, id_consulta ");
		queryStr.append("from paciente where fue_atendido = false ");
		queryStr.append("order by fecha_llegada asc");
		
		Query query = em.createNativeQuery(queryStr.toString(), "listarPacientesPorLlegada");
		List<PacienteDto> pacientesList = null;
		try {
			pacientesList = query.getResultList();
		}
		catch(Exception ex) {
			log.info("Error en persistencia: " + ex.getMessage());
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Error en persistencia");
		}
		
		return pacientesList;
	}
	
}
