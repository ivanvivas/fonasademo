package cl.fonasa.service.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.fonasa.service.model.ConsultaModel;

@Repository
public interface ConsultaRepository extends JpaRepository<ConsultaModel, Serializable>, ConsultaRepositoryExtension {

}
