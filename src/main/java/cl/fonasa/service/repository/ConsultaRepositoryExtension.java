package cl.fonasa.service.repository;

import java.util.List;

import cl.fonasa.service.dto.ConsultaDto;
import cl.fonasa.service.model.ConsultaModel;

/**
 * Interface extension de consulta repository
 * 
 * @author exivima
 *
 */
public interface ConsultaRepositoryExtension {
	/**
	 * Interface de repository para consulta de Mas Pacientes Atendidos
	 * 
	 * @return ConsultaDto
	 */
	public List<ConsultaDto> consultaMasPacientesAtendidos();
	
	/**
	 * Interface de repository para obtener consulta disponible
	 * 
	 * @return ConsultaModel
	 */
	public ConsultaModel obtenerConsultaDisponible(Integer idHospital, String tipoConsulta);
	
	/**
	 * Interface de repository para listar Consultas Ocupadas
	 * 
	 * @return List<ConsultaModel>
	 */
	public List<ConsultaModel> listarConsultasOcupadas();
}
