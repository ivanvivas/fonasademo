package cl.fonasa.service.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

import cl.fonasa.service.dto.ConsultaDto;
import cl.fonasa.service.dto.DisponiblesDto;
import cl.fonasa.service.model.ConsultaModel;
import cl.fonasa.service.util.ConstantesUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Clase para implementaciones de interfaces de repository
 * 
 * @author exivima
 *
 */
@Slf4j
public class ConsultaRepositoryExtensionImpl implements ConsultaRepositoryExtension {
	@Autowired
	private EntityManager em;
	
	/**
	 * Implementacion de interface de repository de consultaMasPacientesAtendidos
	 */
	@Override
	public List<ConsultaDto> consultaMasPacientesAtendidos() {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("select id_consulta, cant_pacientes, nombre_especialista, ");
		queryStr.append("tipo_consulta, estado, id_hospital ");
		queryStr.append("from consulta order by cant_pacientes desc limit 1");
		
		Query query = em.createNativeQuery(queryStr.toString(), "consultaMasPacientesAtendidos");
		
		List<ConsultaDto> consultaList;
		try {
			consultaList = query.getResultList();
		}
		catch(Exception ex) {
			log.info("Error en persistencia: " + ex.getMessage());
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Error en persistencia");
		}
		
		return consultaList;
	}

	/**
	 * Implementacion de interface de repository de obtenerConsultaDisponible
	 */
	@Override
	public ConsultaModel obtenerConsultaDisponible(Integer idHospital, String tipoConsulta) {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("select id_consulta, cant_pacientes, nombre_especialista, ");
		queryStr.append("tipo_consulta, estado, id_hospital from consulta ");
		queryStr.append("where estado = \'").append(ConstantesUtil.ESTADO_CONSULTA_LIBRE).append("\'");
		queryStr.append(" and id_hospital = ").append(idHospital);
		queryStr.append(" and tipo_consulta = \'").append(tipoConsulta).append("\' limit 1");
		
		Query query = em.createNativeQuery(queryStr.toString(), "consultaDisponible");
		
		List<ConsultaModel> consultaDisponibleList = new ArrayList<>();
		try {
			consultaDisponibleList = query.getResultList();
		}
		catch(Exception ex) {
			log.info("Error en persistencia: " + ex.getMessage());
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Error en persistencia");
		}
		
		if (consultaDisponibleList.size() == 0) {
			return null;
		}
		
		return consultaDisponibleList.get(0);
	}

	/**
	 * Implementacion de interface de repository de listarConsultasOcupadas
	 */
	@Override
	public List<ConsultaModel> listarConsultasOcupadas() {
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("select id_consulta, cant_pacientes, nombre_especialista, ");
		queryStr.append("tipo_consulta, estado, id_hospital from consulta ");
		queryStr.append("where estado = \'").append(ConstantesUtil.ESTADO_CONSULTA_OCUPADA).append("\'");
		
		Query query = em.createNativeQuery(queryStr.toString(), "consultasOcupadas");
		
		List<ConsultaModel> consultasList = null;
		try {
			consultasList = query.getResultList();
		}
		catch(Exception ex) {
			log.info("Error en persistencia: " + ex.getMessage());
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Error en persistencia");
		}
		
		return consultasList;
	}

}
