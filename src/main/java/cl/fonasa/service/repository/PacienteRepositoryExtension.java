package cl.fonasa.service.repository;

import java.util.List;

import cl.fonasa.service.dto.PacienteDto;
import cl.fonasa.service.model.PacienteModel;

/**
 * Interface extension de paciente repository
 * 
 * @author exivima
 *
 */
public interface PacienteRepositoryExtension {

	/**
	 * Interface de repository para lista de pacientes fumadores urgentes
	 * 
	 * @return List<PacienteDto>
	 */
	public List<PacienteDto> listarPacientesFumadoresUrgentes();
	
	/**
	 * Interface de repository para obtener paciente mas anciano
	 * 
	 * @return List<PacienteDto>
	 */
	public List<PacienteDto> pacienteMasAnciano();
	
	/**
	 * Interface de repository para lista de pacientes de mayor riesgo
	 * 
	 * @param Integer historiaClinica
	 * @return List<PacienteDto>
	 */
	public List<PacienteDto> listarPacientesMayorRiego(Integer historiaClinica);
	
	/**
	 * Interface de repository para lista de pacientes ordenados por prioridad
	 * 
	 * @return List<PacienteDto>
	 */
	public List<PacienteModel> listarPacientesPorPrioridad();
	
	/**
	 * Interface de repository para obtener lista de pacientes en espera
	 * 
	 * @return List<PacienteDto>
	 */
	public List<PacienteDto> listarPacientesEnEspera();
}
